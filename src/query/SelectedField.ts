import NameResolution from "./naming/NameResolution.js";

export default class SelectedField {
    protected readonly field: string;
    protected readonly alias: string|undefined;

    constructor(field: string, alias: string|undefined = undefined) {
        this.field = field;
        this.alias = alias;
    }

    toString(nameResolution: NameResolution|undefined = undefined): string {
        const field = NameResolution.resolveFieldName(this.field, nameResolution);

        if (this.alias) {
            return `(${field} AS ?${this.alias})`;
        } else {
            return field;
        }
    }
}