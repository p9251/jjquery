/**
 * Generates names to be used in queries
 * in order to avoid name collisions.
 */
import {Resource} from "../../rdf/classes/resources.js";
import ResolvedRelationship from "./ResolvedRelationship.js";
import {Relation} from "../../rdf/relation.js";
import {ICondition} from "../condition/ICondition.js";

export default class NameResolution {
    protected model: Resource;

    constructor(model: Resource) {
        this.model = model;
    }

    protected relations: {
        [key: string]: ResolvedRelationship;
    } = {};

    public getRelationshipForFieldName(fieldName: string): ResolvedRelationship|undefined {
        return Object.values(this.relations).find((r) => r.fieldName === fieldName);
    }

    public getRelationshipForAlias(alias: string): ResolvedRelationship|undefined {
        return this.relations[alias];
    }

    protected async addRelationWithAlias(relation: string, alias: string): Promise<ResolvedRelationship> {
        const fieldName = this.getRelationshipFieldName(alias);
        const modelPath = this.resolveModelPath(relation);
        const modelRelation = await this.resolveRelationship(modelPath);
        const model = await modelRelation.getModelInstance();

        if (this.relations[alias] !== undefined) {
            throw new Error(`Relation ${alias} already exists`);
        }
        return this.relations[alias] = new ResolvedRelationship(relation, alias, fieldName, modelPath, model);
    }

    async addRelation(relation: string, alias: string|undefined = undefined): Promise<ResolvedRelationship> {
        if (alias) {
            return this.addRelationWithAlias(relation, alias);
        } else {
            return this.addRelationWithAlias(relation, relation);
        }
    }

    isRelationshipFieldName(fieldName: string): boolean {
        return this.relations[fieldName] !== undefined;
    }

    isPropertyFieldName(property: string): boolean {
        const match = property.match(/^[a-zA-Z_][a-zA-Z0-9_]*(\.[a-zA-Z_][a-zA-Z0-9_]*)*$/);
        return match !== null;
    }

    getPropertyName(property: string): string {
        if (['_iri', '_type'].includes(property)) {
            //Ignore special fields
            return property;
        }

        if (property.startsWith('p_')) {
            //Already a property name
            return property;
        }

        //Split property on '.'
        const parts = property.split('.');
        const propertyName = parts.pop();
        if (parts.length > 0) {
            const relations = parts.join('_');
            return `r_${relations}_p_${propertyName}`;
        } else {
            return `p_${propertyName}`;
        }
    }

    getRelationshipFieldName(relation: string): string {
        return 'r_' + relation.replace('.', '_');
    }

    resolveRelationshipFieldName(fieldName: string): ResolvedRelationship|undefined {
        //Convert the query field name back to its alias to find resolved relationship

        //Remove the 'r_' prefix
        const relation = fieldName.substring(2);

        //Replace the '_'s with '.'s
        const alias = relation.replace('_', '.');
        return this.relations[alias];
    }

    isPropertyName(name: string): boolean {
        return name.startsWith('p_') || name.includes('_p_');
    }

    isRelationshipName(name: string): boolean {
        return name.startsWith('r_') && !this.isPropertyName(name);
    }

    isRelatedFieldName(name: string): boolean {
        return name.startsWith('r_');
    }

    resolveModelPath(relation: string): string[] {
        const parts = relation.split('.');

        //All but the last part must exist as an alias already
        if (parts.length == 1) {
            //todo: probably a bug here with aliases
            return parts;
        } else {
            const lastPart = <string> parts.pop();
            const resolved = this.relations[parts.join('.')];
            if (!resolved) {
                throw new Error(`Could not resolve model path for relation ${relation}`);
            }
            return [...resolved.modelPath, lastPart];
        }
    }

    async resolveRelationship(relatedPath: string[]): Promise<Relation<Resource>> {
        let model: Resource|undefined = this.model;
        let relation: Relation<Resource>|undefined = undefined;
        for (const part of relatedPath) {
            relation = model.getRelationship(part);
            if (!relation) {
                break;
            }
            model = await relation.getModelInstance();
        }
        if (!relation) {
            throw new Error(`Could not resolve relationship for path ${relatedPath}`);
        }
        return relation;
    }

    getPartsFromFieldName(fieldName: string): [string, string] | undefined {
        const parts = fieldName.split('_p_');
        if (parts.length == 2) {
            const relation = (<string>parts[0]).substring(2);
            const property = <string> parts[1];
            return [relation, property];
        } else {
            return undefined;
        }
    }

    getPropertyFieldName(relation: string, property: string): string {
        return this.getRelationshipFieldName(relation) + '_p_' + property;
    }

    static resolveFieldName(fieldName: ICondition, nameResolution: NameResolution|undefined = undefined): string {
        if (typeof fieldName === 'string' && nameResolution) {
            if (nameResolution.isRelationshipFieldName(fieldName)) {
                return '?' + nameResolution.getRelationshipFieldName(fieldName);
            } else if (nameResolution.isPropertyFieldName(fieldName)) {
                return '?' + nameResolution.getPropertyName(fieldName);
            } else {
                return fieldName;
            }
        } else {
            return fieldName.toString(nameResolution);
        }
    }

    substituteFieldNames(value: string) {
        //Find all field names in the value
        const matches = value.matchAll(/(\\*)\{([a-zA-Z_][a-zA-Z0-9_]*(\.[a-zA-Z_][a-zA-Z0-9_]*)*)\}/g);
        for (const match of matches) {
            const substitution = match[0];
            const escaped = match[1] ?? '';
            const fieldName = match[2];

            if (substitution && fieldName) {
                //The number of \ in the final string
                const escapeCount = Math.floor(escaped.length / 2);
                const escapePrefix = '\\'.repeat(escapeCount);
                let resolution = '';

                if (escaped.length % 2 === 0) {
                    //The substitution is not escaped
                    resolution = escapePrefix + NameResolution.resolveFieldName(fieldName, this);
                } else {
                    //The substitution is escaped
                    resolution = escapePrefix + substitution.substring(escaped.length);
                }
                value = value.replace(substitution, resolution);
            }
        }

        return value;
    }
};