import {Resource} from "../../rdf/classes/resources.js";

export default class ResolvedRelationship {
    //The name of the relationship
    public readonly name: string;

    //The alias of the relationship in the query
    public readonly alias: string;

    //The field name used in the query
    public readonly fieldName: string;

    //The path from the original model to the related model
    public readonly modelPath: string[];

    //The model that the relationship is for
    public readonly model: Resource;

    constructor(name: string, alias: string, fieldName: string, modelPath: string[], model: Resource) {
        this.name = name;
        this.alias = alias;
        this.fieldName = fieldName;
        this.modelPath = modelPath;
        this.model = model;
    }
}