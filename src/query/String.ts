import {ICondition} from "./condition/ICondition.js";
import DataFormatter from "../rdf/types/DataFormatter.js";

/**
 * This represents a string that will be used in the query quoted
 * and without any variable substitution or other fancy stuff.
 */
class String implements ICondition {
    private readonly value: string;

    constructor(value: string) {
        this.value = value;
    }

    toString(): string {
        return DataFormatter.formatAsRdf(this.value, 'string') ?? '""';
    }
};

const Str = (value: string): String => new String(value);

export { Str };
export default String;

