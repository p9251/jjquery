import Triple from "../../rdf/Triple.js";
import NameResolution from "../naming/NameResolution.js";

export default class SimpleFilterCondition extends Triple {
    toString(nameResolution: NameResolution | undefined): string {
        return `( ${super.toString(nameResolution)} )`;
    }
}