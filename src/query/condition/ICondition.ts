import NameResolution from "../naming/NameResolution.js";

export interface ICondition {
    toString(nameResolution: NameResolution | undefined | number): string;
}