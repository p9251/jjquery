/**
 * A helper class for building conditions.
 *
 * Examples:
 * new ConditionBuilder('name').eq('Bob');
 *  or in shorthand: CB('name').eq('Bob')
 * CB('age').gt(18)
 * CB('age').between(18, 65)
 * CB('name').like('/Freud/') -- regex match
 * CB('name').in(['Bob', 'Alice'])
 * CB('_iri').a('Agent')
 * CB('_iri').subClassOf('CreativeWork')
 * CB('author.name').eq('Bob') -- related property
 * CB('?r_author_p_name').eq('Bob') -- related property using resolved name directly
 * CB('author').isNull()
 * CB('author').isNotNull()
 *
 * AND/OR:
 * CB('name').eq('Bob').and('age').gt(18)
 * CB('name').eq('Bob').or('age').gt(18)
 */
import {Resource} from "../../rdf/classes/resources.js";
import {ICondition} from "./ICondition.js";
import SimpleFilterCondition from "./SimpleFilterCondition.js";
import SparqlFunction from "../SparqlFunction.js";
import NotCondition from "./NotCondition.js";
import {QueryBuilder} from "../QueryBuilder.js";
import FilterCondition from "./FilterCondition.js";
import { Exp } from "../Expression.js";

class ConditionBuilder implements ICondition {
    private subject: ICondition;
    private condition: ICondition|undefined = undefined;
    private mode: '||'|'&&'|'' = '';
    private queryBuilder: QueryBuilder|undefined = undefined;

    constructor(subject: ICondition, queryBuilder: QueryBuilder|undefined = undefined) {
        if (typeof subject === 'string') {
            subject = Exp('{' + subject + '}');
        }
        this.subject = subject;
        this.queryBuilder = queryBuilder;
    }

    private consolidateCondition(condition: ICondition): ConditionBuilder {
        if (this.mode !== '' && this.condition) {
            this.condition = new SimpleFilterCondition(this.condition, this.mode, condition);
            this.mode = '';
        } else {
            this.condition = condition;
        }
        return this;
    }

    //The basic operators
    subClassOf(className: any): ConditionBuilder {
        if (className instanceof Resource) {
            className = className.constructor.name;
        }
        return this.consolidateCondition(new SimpleFilterCondition(this.subject, 'rdfs:subClassOf*', ':' + className));
    }

    a(className: any): ConditionBuilder {
        if (className instanceof Resource) {
            className = className.constructor.name;
        }
        return this.consolidateCondition(new SimpleFilterCondition(this.subject, 'a', ':' + className));
    }

    eq(object: ICondition): ConditionBuilder {
        //todo: handle data types
        //Only use the last part of the subject for the relationship name
        return this.consolidateCondition(new SimpleFilterCondition(this.subject, '=', object));
    }

    neq(object: ICondition): ConditionBuilder {
        return this.consolidateCondition(new SimpleFilterCondition(this.subject, '!=', object));
    }

    gt(object: ICondition): ConditionBuilder {
        return this.consolidateCondition(new SimpleFilterCondition(this.subject, '>', object));
    }

    gte(object: ICondition): ConditionBuilder {
        const greaterThan = new SimpleFilterCondition(this.subject, '>', object);
        const equalTo = new SimpleFilterCondition(this.subject, '=', object);
        return this.consolidateCondition(new SimpleFilterCondition(greaterThan, '||', equalTo));
    }

    lt(object: ICondition): ConditionBuilder {
        return this.consolidateCondition(new SimpleFilterCondition(this.subject, '<', object));
    }

    lte(object: ICondition): ConditionBuilder {
        const lessThan = new SimpleFilterCondition(this.subject, '<', object);
        const equalTo = new SimpleFilterCondition(this.subject, '=', object);
        return this.consolidateCondition(new SimpleFilterCondition(lessThan, '||', equalTo));
    }

    in(object: ICondition): ConditionBuilder {
        return this.consolidateCondition(new SimpleFilterCondition(this.subject, 'IN', object));
    }

    notIn(object: ICondition): ConditionBuilder {
        return this.consolidateCondition(new SimpleFilterCondition(this.subject, 'NOT IN', object));
    }

    like(pattern: string, caseInsensitive: boolean = false): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('regex', this.subject, pattern, caseInsensitive ? 'i' : ''));
    }

    notLike(pattern: string, caseInsensitive: boolean = false): ConditionBuilder {
        return this.consolidateCondition(
            new NotCondition(
                new SparqlFunction('regex', this.subject, pattern, caseInsensitive ? 'i' : '')
            )
        );
    }

    between(n1: number, n2: number): ConditionBuilder {
        const greaterThan = new SimpleFilterCondition(this.subject, '>', n1);
        const lessThan = new SimpleFilterCondition(this.subject, '<', n2);
        return this.consolidateCondition(new SimpleFilterCondition(greaterThan, '&&', lessThan));
    }

    notBetween(n1: number, n2: number): ConditionBuilder {
        const greaterThan = new SimpleFilterCondition(this.subject, '>', n1);
        const lessThan = new SimpleFilterCondition(this.subject, '<', n2);
        return this.consolidateCondition(
            new NotCondition(new SimpleFilterCondition(greaterThan, '&&', lessThan))
        );
    }

    //Mathematical operators
    add(object: ICondition): ConditionBuilder {
        return this.consolidateCondition(new SimpleFilterCondition(this.subject, '+', object));
    }

    subtract(object: ICondition): ConditionBuilder {
        return this.consolidateCondition(new SimpleFilterCondition(this.subject, '-', object));
    }

    multiply(object: ICondition): ConditionBuilder {
        return this.consolidateCondition(new SimpleFilterCondition(this.subject, '*', object));
    }

    divide(object: ICondition): ConditionBuilder {
        return this.consolidateCondition(new SimpleFilterCondition(this.subject, '/', object));
    }

    abs(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('abs', this.subject));
    }

    round(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('round', this.subject));
    }

    ceil(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('ceil', this.subject));
    }

    floor(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('floor', this.subject));
    }


    //Function conditions
    isNull(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('blank', this.subject));
    }

    isNotNull(): ConditionBuilder {
        return this.consolidateCondition(
            new NotCondition(new SparqlFunction('blank', this.subject))
        );
    }

    isIRI(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('isIRI', this.subject));
    }

    isBlank(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('isBlank', this.subject));
    }

    isLiteral(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('isLiteral', this.subject));
    }

    isNumeric(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('isNumeric', this.subject));
    }

    //String Functions
    strlen(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('strlen', this.subject));
    }

    substr(start: number, length: number): ConditionBuilder {
        //todo: verify that this is correct
        return this.consolidateCondition(new SparqlFunction('substr', this.subject, start, length));
    }

    strStarts(pattern: string): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('strStarts', this.subject, pattern));
    }

    strEnds(pattern: string): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('strEnds', this.subject, pattern));
    }

    contains(pattern: string): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('contains', this.subject, pattern));
    }

    strBefore(pattern: string): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('strBefore', this.subject, pattern));
    }

    strAfter(pattern: string): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('strAfter', this.subject, pattern));
    }

    upperCase(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('ucase', this.subject));
    }

    lowerCase(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('lcase', this.subject));
    }

    concat(object: ICondition): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('concat', this.subject, object));
    }

    //Hashing functions
    md5(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('md5', this.subject));
    }

    sha1(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('sha1', this.subject));
    }

    sha256(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('sha256', this.subject));
    }

    sha384(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('sha384', this.subject));
    }

    sha512(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('sha512', this.subject));
    }

    //Date functions
    now(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('now'));
    }

    year(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('year', this.subject));
    }

    month(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('month', this.subject));
    }

    day(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('day', this.subject));
    }

    hours(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('hours', this.subject));
    }

    minutes(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('minutes', this.subject));
    }

    seconds(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('seconds', this.subject));
    }

    timezone(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('timezone', this.subject));
    }

    tz(): ConditionBuilder {
        return this.consolidateCondition(new SparqlFunction('tz', this.subject));
    }

    //Combining conditions
    and(subject: ICondition): ConditionBuilder {
        if (typeof subject === 'string') {
            subject = Exp('{' + subject + '}');
        }

        this.subject = subject;
        this.mode = '&&';
        return this;
    }

    or(subject: ICondition): ConditionBuilder {
        if (typeof subject === 'string') {
            subject = Exp('{' + subject + '}');
        }

        this.subject = subject;
        this.mode = '||';
        return this;
    }

    /**
     * Lets the class know that we're finished building the condition and can cleanup any leftovers.
     *
     * This should be called prior to using the condition in a query.
     */
    done(): void {
        //Catch any incomplete/unconsolidated conditions
        if (this.mode !== '' && this.subject) {
            this.consolidateCondition(this.subject);
        }
    }

    /**
     * Finishes with the class and applies to a query builder if one is present.
     */
    apply(): void {
        this.done();
        if (this.queryBuilder) {
            this.queryBuilder.applyFilter(new FilterCondition(this));
        }
    }

    toString(nameResolution: any): string {
        this.done();
        return this.condition ? this.condition.toString(nameResolution) : '';
    }
}


const CB = (subject: string) => new ConditionBuilder(subject);

export { CB };
export default ConditionBuilder;
