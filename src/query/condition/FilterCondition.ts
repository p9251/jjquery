import {ICondition} from "./ICondition.js";
import NameResolution from "../naming/NameResolution.js";

export default class FilterCondition implements ICondition{
    protected condition: ICondition;

    constructor(condition: ICondition) {
        this.condition = condition;
    }

    toString(nameResolution: NameResolution | undefined): string {
        return 'FILTER ' + this.condition.toString(nameResolution);
    }
}