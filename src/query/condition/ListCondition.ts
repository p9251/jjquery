import {ICondition} from "./ICondition.js";
import NameResolution from "../naming/NameResolution.js";

export default class ListCondition implements ICondition {
    protected conditions: ICondition[] = [];

    constructor(conditions: ICondition[]|undefined) {
        if (conditions) {
            this.conditions = conditions;
        }
    }

    addCondition(condition: ICondition) {
        this.conditions.push(condition);
    }

    getConditions(): ICondition[] {
        return this.conditions;
    }

    toString(nameResolution: NameResolution | undefined): string {
        return this.conditions.map((condition) => condition.toString(nameResolution)).join('\n');
    }

    isEmpty() {
        return this.conditions.length == 0;
    }
}