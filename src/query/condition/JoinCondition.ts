import {QueryCondition} from "./QueryCondition.js";
import NameResolution from "../naming/NameResolution.js";
import ListCondition from "./ListCondition.js";

export default class JoinCondition extends QueryCondition {
    protected onConditions: ListCondition = new ListCondition([]);

    constructor(subject: string, predicate: string, object: string, optional: boolean|undefined = false) {
        super(subject, predicate, object, optional);
    }

    addOnCondition(condition: QueryCondition): void {
        this.onConditions.addCondition(condition);
    }

    toString(nameResolution: NameResolution | undefined): string {
        if (this.onConditions.isEmpty()) {
            return super.toString(nameResolution);
        } else {
            //If we have a list of on conditions, we convert to a list condition and ensure that the first part is non-optional, and instead apply the optional to the entire list
            const joinConditions = new ListCondition([new QueryCondition(this.subject, this.predicate, this.object, false), ...this.onConditions.getConditions()]);
            return (this.isOptional() ? 'OPTIONAL ' : '') + `{ ${joinConditions.toString(nameResolution)} }`;
        }
    }
}