import {ICondition} from "./ICondition.js";
import NameResolution from "../naming/NameResolution.js";

export default class NotCondition implements ICondition {
    protected subject: ICondition;

    constructor(subject: ICondition) {
        this.subject = subject;
    }

    toString(nameResolution: NameResolution | undefined): string {
        return `!(${NameResolution.resolveFieldName(this.subject, nameResolution)})`;
    }
}