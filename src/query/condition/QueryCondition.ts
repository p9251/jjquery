import Triple from "../../rdf/Triple.js";
import NameResolution from "../naming/NameResolution.js";
import {ICondition} from "./ICondition.js";

export class QueryCondition extends Triple {
    protected optional: boolean = false;

    constructor(subject: ICondition, predicate: ICondition, object: ICondition, optional: boolean|undefined = false) {
        super(subject, predicate, object);
        this.optional = optional;
    }

    isOptional(): boolean {
        return this.optional;
    }

    toString(nameResolution: NameResolution | undefined): string {
        return (this.isOptional() ? 'OPTIONAL ' : '') + `{ ${super.toString(nameResolution)} }`;
    }
}