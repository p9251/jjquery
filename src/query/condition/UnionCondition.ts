import {ICondition} from "./ICondition.js";
import NameResolution from "../naming/NameResolution.js";

export class UnionCondition implements ICondition {
    protected conditionA: ICondition;
    protected conditionB: ICondition;

    constructor(conditionA: ICondition, conditionB: ICondition) {
        this.conditionA = conditionA;
        this.conditionB = conditionB;
    }

    toString(nameResolution: NameResolution | undefined): string {
        return `{ ${this.conditionA.toString(nameResolution)} UNION ${this.conditionB.toString(nameResolution)} }`;
    }
}