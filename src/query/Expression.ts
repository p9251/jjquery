import {ICondition} from "./condition/ICondition.js";
import NameResolution from "./naming/NameResolution.js";

/**
 * Represents a query expression that can be used in a WHERE (or maybe even a SELECT) clause.
 * This class will perform field name substitution as follows:
 * {age} => ?p_age
 * {person.age} => ?r_person_p_age
 * \{age} => {age}
 * \\{age} => \?p_age
 * \\\{age} => \{age}
 * \\\\{age} => \\?p_age
 */
class Expression implements ICondition {
    private readonly value: string;

    constructor(value: string) {
        this.value = value;
    }

    toString(nameResolution: NameResolution|undefined = undefined): string {
        return nameResolution ?  nameResolution.substituteFieldNames(this.value) : this.value;
    }
}

const Exp = (value: string): Expression => new Expression(value);

export { Exp };
export default Expression;