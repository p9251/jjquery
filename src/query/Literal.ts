import {ICondition} from "./condition/ICondition.js";

/**
 * This represents a literal (such as an IRI) that will be used in a query unquoted
 * and without any variable substitution or other fancy stuff.
 */
class Literal implements ICondition {
    private readonly value: string;

    constructor(value: string) {
        this.value = value;
    }

    toString(): string {
        return this.value;
    }
}

const Lit = (value: string): Literal => new Literal(value);

export { Lit };
export default Literal;