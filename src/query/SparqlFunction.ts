import {ICondition} from "./condition/ICondition.js";
import NameResolution from "./naming/NameResolution.js";

export default class SparqlFunction implements ICondition {
    private readonly name: string;
    private readonly args: ICondition[];

    constructor(name: string, ...args: ICondition[]) {
        this.name = name;
        this.args = args;
    }

    toString(nameResolution: NameResolution | undefined): string {
        return `${this.name}(${this.args.map(arg => NameResolution.resolveFieldName(arg, nameResolution)).join(', ')})`;
    }
}