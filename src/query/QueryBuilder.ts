import {ICondition} from "./condition/ICondition.js";
import ITriple from "../rdf/ITriple.js";
import Triple from "../rdf/Triple.js";
import {QueryCondition} from "./condition/QueryCondition.js";
import {Resource} from "../rdf/classes/resources.js";
import NameResolution from "./naming/NameResolution.js";
import JoinCondition from "./condition/JoinCondition.js";
import ConditionBuilder from "./condition/ConditionBuilder.js";
import SelectedField from "./SelectedField.js";

export class QueryBuilder {
    protected nameResolution: NameResolution;
    protected model: Resource;
    protected method: string = 'SELECT';
    protected selectedFields: SelectedField[] = [];
    protected where: ICondition[] = [];
    protected deleteTriples: ITriple[] = [];
    protected insertTriples: ITriple[] = [];
    protected joinConditions: { [key: string]: JoinCondition } = {};

    constructor(model: Resource) {
        this.model = model;
        this.nameResolution = new NameResolution(model);
    }

    protected prefixes: { [key: string]: string } = {
        owl: '<http://www.w3.org/2002/07/owl#>',
        rdf: '<http://www.w3.org/1999/02/22-rdf-syntax-ns#>',
        rdfs: '<http://www.w3.org/2000/01/rdf-schema#>',
        xsd: '<http://www.w3.org/2001/XMLSchema#>',
    };

    addPrefix(prefix: string, schema: string): QueryBuilder {
        this.prefixes[prefix] = schema;
        return this;
    }

    addCondition(condition: ICondition): QueryBuilder {
        this.where.push(condition);
        return this;
    }

    async join(relation: string, alias: string = relation, optional: boolean = false, joinWith: boolean = false): Promise<QueryBuilder> {
        const resolved = await this.nameResolution.addRelation(relation, alias);

        //Hmm, this could probably just add the condition to the where array
        //But we need the model information to do that

        //Get the relationship from the model
        const relatedPath = this.nameResolution.resolveModelPath(relation);
        let relationship = await this.nameResolution.resolveRelationship(relatedPath);
        if (!relationship) {
            throw new Error(`No relation found for ${relation}`);
        }

        //Get the parts of the relationship
        let subject = '?_iri';

        const relationshipParts = relation.split('.');
        if (relationshipParts.length > 1) {
            //We're joining to a relationship, not this model
            subject = `?${resolved.fieldName}`;
        }

        //Get the predicate
        //todo: use the correct subject/predicate for relations (won't be _iri)
        let predicate = ':' + relationship.getPredicate();
        let object = '?' + resolved.fieldName;

        if (relationship.getPredicate().startsWith('-')) {
            //Switch the subject and object
            object = subject;
            subject = '?' + resolved.fieldName;
            predicate = ':' + relationship.getPredicate().substring(1);
        }

        const joinCondition = new JoinCondition(subject, predicate, object, optional);

        if (joinWith) {
            //Select the iri/id of the related model
            this.addSelect(resolved.fieldName);

            //Select and query for the type of the related model
            this.addSelect(this.nameResolution.getPropertyFieldName(alias, '_type'));

            joinCondition.addOnCondition(new QueryCondition('?' + resolved.fieldName, 'a', '?' + this.nameResolution.getPropertyFieldName(alias, '_type')))


            for (let prop of Object.keys(resolved.model.getProperties())) {
                const relatedProperty = this.nameResolution.getPropertyFieldName(alias, prop);
                this.addSelect(relatedProperty);
                joinCondition.addOnCondition(new QueryCondition('?' + resolved.fieldName, ':' + prop, '?' + relatedProperty, true));
            }
        }

        if (relationshipParts.length > 1) {
            //Get all but the last part
            relationshipParts.pop();
            const baseRelationship = relationshipParts.join('.');
            const baseJoinCondition = this.joinConditions[baseRelationship];
            if (baseJoinCondition) {
                baseJoinCondition.addOnCondition(joinCondition);
            } else {
                throw new Error(`No join condition found for ${baseRelationship}`);
            }
        } else {
            this.addCondition(joinCondition);
        }
        this.joinConditions[alias] = joinCondition;

        return this;
    }

    async with(relation: string, alias: string = relation, optional: boolean = false): Promise<QueryBuilder> {
        return this.join(relation, alias, optional, true);
    }

    filter(filter: ICondition): ConditionBuilder {
        return new ConditionBuilder(filter, this);
    }

    applyFilter(filter: ICondition): QueryBuilder {
        this.where.push(filter);
        return this;
    }

    addSelect(field: string, alias: string|undefined = undefined): QueryBuilder {
        this.selectedFields.push(new SelectedField(field, alias));
        return this;
    }

    addDelete(deleteTriple: ITriple): QueryBuilder {
        this.deleteTriples.push(deleteTriple);
        return this.delete();
    }

    updateTriple(triple: ITriple): QueryBuilder {
        if (triple.getObject() !== null) {
            //nulls mean we have removed the property/relationship
            this.insertTriples.push(triple);
        }

        let predicate = triple.getPredicate();
        predicate = predicate.toString(undefined).replace(/.*:/, '');
        this.deleteTriples.push(new Triple(triple.getSubject(), triple.getPredicate(), `?${predicate}`));
        this.where.push(new QueryCondition(triple.getSubject(), '?p', `?${predicate}`));
        this.method = 'UPDATE';
        return this;
    }

    insertTriple(triple: ITriple): QueryBuilder {
        this.method = 'INSERT';
        this.insertTriples.push(triple);
        return this;
    }

    select(): QueryBuilder {
        this.method = 'SELECT';
        return this;
    }

    insert(): QueryBuilder {
        this.method = 'INSERT';
        return this;
    }

    delete(): QueryBuilder {
        this.method = 'DELETE';
        return this;
    }

    update(): QueryBuilder {
        this.method = 'UPDATE';
        return this;
    }

    getMethod(): string {
        return this.method;
    }

    toSparql(): string {
        let sparql = '';

        //Add prefixes
        Object.keys(this.prefixes).forEach((prefix: string) => {
            sparql += `PREFIX ${prefix}: ${this.prefixes[prefix]}\n`;
        });
        sparql += '\n';

        if (this.method == 'SELECT') {
            //Add selected fields
            const fields: string[] = [];
            this.selectedFields.forEach((field: SelectedField, fieldIdx) => {
                fields[fieldIdx] = field.toString(this.nameResolution);
            });
            sparql += 'SELECT ' + fields.join(' ') + '\n';
        }

        if (this.method == 'DELETE' || this.method == 'UPDATE') {
            sparql += 'DELETE {\n';
            sparql += this.deleteTriples.map((triple) => '\t' + triple.toString()).join(' .\n');
            sparql += '\n}\n'
        } if (this.method == 'INSERT' || (this.method == 'UPDATE' && this.insertTriples.length > 0)) {
            sparql += `INSERT ${this.method == 'INSERT' ? 'DATA ' : ''}{\n`;
            sparql += this.insertTriples.map((triple) => '\t' + triple.toString()).join(' .\n');
            sparql += '\n}\n';
        }

        //Add conditions (todo: make filters work in the wheres and below!)
        if (this.where.length > 0) {
            sparql += 'WHERE {\n';

            //Remove nested filter conditions (weird solution to not duplicate the filters)
            const where = this.where.filter((condition) => condition);

            sparql += where.map((condition) => '\t' + condition.toString(this.nameResolution)).join('\n');
            sparql += '\n}\n';
        }

        return sparql;
    }

    getNameResolution(): NameResolution {
        return this.nameResolution;
    }
}