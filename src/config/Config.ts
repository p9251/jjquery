import SparqlClient from "sparql-http-client";

export class Config {
    public endpointUrl: string;
    public graph: string|undefined;
    public prefixes: {[key: string]: string} = {};
    public headers: {[key: string]: string} = {};

    public getOptions(): SparqlClient.StreamClientOptions {
        const options: SparqlClient.StreamClientOptions = {
            endpointUrl: this.endpointUrl
        };
        options.headers = this.headers;
        return options;
    }
}