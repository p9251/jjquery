import {Resource} from "../rdf/classes/resources.js";
import * as fs from "fs";

export default class ClassLoader {
    protected directories: string[] = [];
    protected loadedClasses: {[key: string]: new () => Resource} = {};

    constructor(directories: string[]) {
        this.directories = directories;
    }

    getLoadedClasses(): {[key: string]: new () => Resource} {
        return this.loadedClasses;
    }

    addDirectory(directory: string) {
        this.directories.push(directory);
    }

    newInstance(className: string): Resource {
        if (className === 'Resource') {
            return new Resource();
        }

        if (this.loadedClasses[className] === undefined) {
            throw new Error(`JJQuery: Model ${className} could not be found`);
        }
        //@ts-ignore
        return new this.loadedClasses[className]();
    }

    async newInstanceAsync(className: string): Promise<Resource> {
        if (className === 'Resource') {
            return new Resource();
        }
        await this.load(className);
        //@ts-ignore
        return new this.loadedClasses[className]();
    }

    async load(className: string): Promise<void> {
        if (className === 'Resource') {
            return;
        }

        if (this.loadedClasses[className] === undefined) {
            for (const directory of this.directories) {
                const file = `${directory}/${className}.js`;
                const filePath = file.replace('file:///', '');
                if (fs.existsSync(filePath)) {
                    this.loadedClasses[className] = (await import(file)).default;
                    break;
                }
            }
        }

        if (this.loadedClasses[className] === undefined) {
            throw new Error(`JJQuery: Model ${className} could not be loaded, tried: ${this.directories.join(" :: ")}`);
        }
    }
}