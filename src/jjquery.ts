import SparqlClient from 'sparql-http-client';
import {Config} from "./config/Config.js";
import {QueryBuilder} from "./query/QueryBuilder.js";
import ClassLoader from "./config/ClassLoader.js";

export class JJQuery {
    private static classLoader: ClassLoader;
    private readonly client: SparqlClient;
    private readonly config: Config;

    static classDirectories: string[] = [];

    static getClassLoader(): ClassLoader {
        if (!JJQuery.classLoader) {
            JJQuery.classLoader = new ClassLoader(['./models', ...this.classDirectories]);
        }
        return JJQuery.classLoader;
    }

    constructor(endpointUrl: string|undefined, config: Config|undefined) {
        if (!endpointUrl && (!config || !config.endpointUrl)) {
            throw new Error('Either the endpointUrl or the config parameter must be set.');
        }

        if (!config) {
            this.config = new Config();
        } else {
            this.config = config;
        }

        if (endpointUrl) {
            this.config.endpointUrl = endpointUrl;
        }

        this.client = new SparqlClient({ endpointUrl: this.config.endpointUrl, headers: this.config.headers, updateUrl: this.config.endpointUrl });
    }

    getConfig(): Config {
        return this.config;
    }

    async query(queryBuilder: QueryBuilder): Promise<any[]> {
        const sparql = queryBuilder.toSparql();
        return this.queryRaw(sparql, queryBuilder.getMethod());
    }

    async queryRaw(sparql: string, method: string): Promise<any> {
        console.info('JJQuery: sending SPARQL query\n<<<\n' + sparql + '\n>>>');

        if (method == 'SELECT') {
            const stream = await this.client.query.select(sparql);

            return new Promise((resolve, reject) => {
                const results: any[] = [];

                stream.on('data', (row: { [s: string]: any; } | ArrayLike<unknown>) => {
                    results.push(row);
                });

                stream.on('end', () => {
                    resolve(results);
                });

                stream.on('error', (error: any) => {
                    console.error(error);
                    reject(error);
                });
            });
        } else {
            return await this.client.query.update(sparql);
        }
    }
}