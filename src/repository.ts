import {Resource} from "./rdf/classes/resources.js";
import {JJQuery} from "./jjquery.js";
import {QueryBuilder} from "./query/QueryBuilder.js";
import {QueryCondition} from "./query/condition/QueryCondition.js";
import SimpleFilterCondition from "./query/condition/SimpleFilterCondition.js";
import {ICondition} from "./query/condition/ICondition.js";
import FilterCondition from "./query/condition/FilterCondition.js";
import Triple from "./rdf/Triple.js";
import DataFormatter from "./rdf/types/DataFormatter.js";
import ObjectID from "bson-objectid";
import {Relation} from "./rdf/relation.js";
import TypeMapping from "./rdf/types/TypeMapping.js";

export default class Repository<T extends Resource> {
    private readonly className: string;
    private readonly connection: JJQuery;

    constructor(className: string, connection: JJQuery) {
        if (!className) {
            throw new Error('Class name is required');
        }

        this.className = className;
        this.connection = connection;
    }

    public async getQueryBuilder(update: boolean = false): Promise<QueryBuilder> {
        const type = this.className;
        const template = await this.newInstance();
        const props = Object.keys(template.getProperties());
        const queryBuilder = new QueryBuilder(template);

        //Add prefixes
        for (const prefix of Object.keys(this.connection.getConfig().prefixes)) {
            const schema = this.connection.getConfig().prefixes[prefix];
            if (schema) {
                queryBuilder.addPrefix(prefix, schema);
            }
        }

        //Updates don't select or have starting conditions
        if (!update) {
            queryBuilder
                .addSelect('_iri')
                .addSelect('_type')
                .addCondition(new QueryCondition('?_iri', 'a', '?_type'))
                .addCondition(new QueryCondition('?_type', 'rdfs:subClassOf*', `:${type}`))
            ;

            for (const prop of props) {
                queryBuilder.addSelect('p_' + prop);
                queryBuilder.addCondition(new QueryCondition('?_iri', `:${prop}`, `?p_${prop}`, true));
            }
        }

        return queryBuilder;
    }

    async findAll(condition: ICondition|undefined = undefined): Promise<T[]> {
        const queryBuilder = await this.getQueryBuilder();
        if (condition) {
            queryBuilder.addCondition(condition);
        }
        const results = await this.connection.query(queryBuilder);
        return this.loadModels(results);
    }

    //@ts-ignore
    async findByIri(iri: string): Promise<T|undefined> {
        const queryBuilder = await this.getQueryBuilder();
        queryBuilder.addCondition(new FilterCondition(
            new SimpleFilterCondition('?_iri', '=', `:${iri}`)
        ));
        const results = await this.connection.query(queryBuilder);
        const models = await this.loadModels(results);
        return models[0];
    }

    /**
     * Finds models from a completely raw SPARQL query.
     * You really need to know what you're doing here.
     * As far as the names of the properties go and whatnot.
     *
     * @param sparql
     */
    async findBySparql(sparql: string) {
        const results = await this.connection.queryRaw(sparql, 'SELECT');
        return this.loadModels(results);
    }

    /**
     * Finds models using a raw SPARQL WHERE clause.
     * This is a little easier to work with, but you still have to know
     * how the properties are named.
     *
     * @param where
     */
    async findBySparqlWhere(where: string) {
        const queryBuilder = await this.getQueryBuilder();
        const results = await this.connection.queryRaw(queryBuilder.toSparql() + '\nWHERE {\n' + where + '\n}\n', 'SELECT');
        return this.loadModels(results);
    }

    async create(model: T, reload: boolean = false): Promise<T> {
        if (model.iri === '') {
            model.iri = new ObjectID().toHexString();;
        }

        const queryBuilder = await this.getQueryBuilder(true);
        queryBuilder.insertTriple(new Triple(`:${model.iri}`, 'a', `:${model.getType()}`));

        for (const prop of Object.keys(model.getProperties())) {
            let value = model.getProperty(prop);
            if (value === null || value === undefined) {
                //The value isn't set, so don't insert anything
                continue;
            }
            let rdfType = model.getPropertyType(prop);
            if (rdfType === undefined) {
                //We don't know about this type, so let's make a best guess
                rdfType = TypeMapping.guessRdfType(value);
            }

            value = DataFormatter.formatAsRdf(model.getProperty(prop), rdfType)
            queryBuilder.insertTriple(new Triple(`:${model.iri}`, `:${prop}`, value));
        }

        await this.connection.query(queryBuilder.insert());

        let savedModel = model;
        if (reload) {
            savedModel = <T> await this.findByIri(model.iri);
        }
        savedModel.setNew(false);
        return savedModel;
    }

    async update(model: T, reload: boolean = false): Promise<T> {
        const queryBuilder = await this.getQueryBuilder(true);
        const modifiedProperties = Object.keys(model.getModifiedProperties());

        if (modifiedProperties.length > 0) {
            for (const prop of modifiedProperties) {
                let value = model.getProperty(prop);
                const rdfType = model.getPropertyType(prop);
                if (rdfType === undefined) {
                    //We don't know about this type, so skip.
                    continue;
                }
                value = DataFormatter.formatAsRdf(model.getProperty(prop), rdfType)
                queryBuilder.updateTriple(new Triple(`:${model.iri}`, `:${prop}`, value));
            }

            await this.connection.query(queryBuilder.update());
        }

        let savedModel = model;
        if (reload) {
            savedModel = <T> await this.findByIri(model.iri);
        } else {
            savedModel.loadProperties(savedModel.getProperties());
        }
        return savedModel;
    }

    /**
     * Saves the model and returns the model.
     * Optionally reloads from the database.
     *
     * @param model
     */
    async save(model: T, reload: boolean|undefined = false): Promise<T> {
        if (model.isNew()) {
            return this.update(model, reload);
        } else {
            return this.create(model, reload);
        }
    }

    /**
     * Deletes a model and all of its properties and relationships
     *
     * @param model
     */
    async delete(model: T): Promise<boolean> {
        return this.deleteByIri(model.iri);
    }

    /**
     * Deletes a model and all relationships and properties.
     *
     * @param model
     */
    async deleteByIri(iri: string): Promise<boolean> {
        const queryBuilder = (await this.getQueryBuilder(true))
            .addDelete(new Triple('?s', '?o', '?p'))
            .addCondition(new Triple('?s', '?o', '?p'))
            .addCondition(new FilterCondition(
                new SimpleFilterCondition(
                    new SimpleFilterCondition('?s', '=', `:${iri}`),
                    '||',
                    new SimpleFilterCondition('?o', '=', `:${iri}`)
                )
            ))
        ;
        await this.connection.query(queryBuilder.delete());
        return true;
    }

    protected async loadModels(results: any): Promise<T[]> {
        const models: T[] = [];
        for (const result of results) {
            const iri = result._iri.value.replace(/.+#/, '');

            //Create a model for the specific class type
            const type = result._type.value.replace(/.+#/, '');
            const model = await this.newInstance(type);
            model.type = type;
            model.iri = iri;

            model.setNew(false);

            //Get model properties
            const properties = this.getModelPropertiesFromQueryResults(result);
            model.loadProperties(properties);

            //Get model relationships
            const relationships = this.getModelRelationshipsFromQueryResults(result);
            for (const relationship of Object.keys(relationships)) {
                const relatedProperties = this.getModelPropertiesFromQueryResults(result, 'r_' + relationship);
                const relatedModel = await JJQuery.getClassLoader().newInstanceAsync(relatedProperties._type);
                relatedModel.iri = relationships[relationship].replace(/.+#/, '');
                relatedModel.type = relatedProperties._type;
                delete relatedProperties._type;
                relatedModel.loadProperties(relatedProperties);
                relatedModel.setNew(false);

                //Find the model's relationship recursively
                let relation: Relation<Resource>|undefined = undefined;
                const relationshipPath = relationship.split('_');
                const relationshipName = <string> relationshipPath.pop();
                let relationModel: Resource = model;
                for (const relationshipName of relationshipPath) {
                    relation = relationModel.getRelationship(relationshipName);
                    if (!relation) {
                        throw new Error(`Could not find relationship for ${relationship}`);
                    }
                    const instance = await relation?.getModelInstance();
                    if (!instance) {
                        throw new Error(`Could not find relationship for ${relationship}`);
                    }
                    relationModel = instance;
                }

                //Get or create the last relation
                const subject = relation ? relation.getSubject() : model;
                relation = relationModel.getRelationship(relationshipName);
                if (!relation) {
                    relation = new Relation(subject, relationshipName, new Repository<typeof relatedModel>(relatedProperties._type, this.connection));
                    relationModel.setRelationship(relationshipName, relation);
                }
                relation.addModel(relatedModel);
            }
            model.loadProperties(model.getProperties());
            models.push(model);
        }
        return models;
    }

    getType(): string {
        return this.className;
    }

    findByCondition(condition: ICondition) {
        return this.findAll(condition);
    }

    async findByQuery(query: QueryBuilder): Promise<T[]> {
        const results = await this.connection.query(query);
        return this.loadModels(results);
    }

    async getTemplate(className: string = this.className): Promise<T> {
        //@ts-ignore
        return JJQuery.getClassLoader().newInstanceAsync(className);
    }

    async newInstance(className: string = this.className): Promise<T> {
        const model = await this.getTemplate(className);

        //Set up the relationships for the model
        for (const relationship of Object.keys(model.getRelationshipLoaders())) {
            const relationshipLoader = model.getRelationshipLoader(relationship);
            if (relationshipLoader) {
                const relation = await relationshipLoader(this.connection, model);
                if (relation) {
                    model.setRelationship(relationship, relation);
                }
            }
        }

        model.setNew(true);
        return model;
    }

    private getModelPropertiesFromQueryResults(result: {[key: string]: any}, relation: string=''): {[key: string]: any} {
        const properties: { [key: string]: any } = {};
        for (let prop of Object.keys(result)) {
            const isRelation = prop.startsWith('r_');
            const originalProp = prop;
            if (relation !== '' && prop.startsWith(relation)) {
                //If we are getting the properties of a relationship, remove the relationship prefix
                // +1 because there is a trailing "_"
                prop = prop.substring(relation.length + 1);
            }
            if (prop.startsWith('p_') && (relation === '' || isRelation)) {
                //Model properties/relations
                properties[prop.substring(2)] = result[originalProp];
            } else if (!prop.startsWith('p_') && !prop.startsWith('r_')) {
                //Custom/user-defined properties and _iri and _type
                properties[prop] = result[originalProp];
            }
        }

        //Cast to the correct data types
        const typedProperties: { [key: string]: any} = {};
        for (const prop of Object.keys(properties)) {
            const value = properties[prop];
            if (value && value.constructor.name === 'Literal') {
                //Cast to the correct data type
                const type = properties[prop].datatype ? properties[prop].datatype.value.replace(/.+#/, '') : 'string';
                typedProperties[prop] = DataFormatter.parseFromRdf(properties[prop].value, type);
            } else {
                //It's something else, and the value needs to have its prefix removed
                typedProperties[prop] = value.value.replace(/.+#/, '');
            }
        }
        return typedProperties;
    }

    private getModelRelationshipsFromQueryResults(result: {[key: string]: any}): {[key: string]: any} {
        const relationships: { [key: string]: any } = {};
        for (let prop of Object.keys(result).sort((key1, key2) => key1.split('_').length - key2.split('_').length)) {
            if (prop.startsWith('r_') && !prop.includes('_p_')) {
                relationships[prop.substring(2)] = result[prop].value;
            }
        }
        return relationships;
    }
}

