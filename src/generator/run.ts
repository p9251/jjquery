import ModelGenerator from "./ModelGenerator.js";

const generate = async () => {
    const ttlFile = process.env.TTL_FILE;
    const outputFolder = process.env.OUTPUT_DIR;
    if (!ttlFile) {
        throw new Error("TTL_FILE environment variable not set");
    }
    if (!outputFolder) {
        throw new Error("OUTPUT_DIR environment variable not set");
    }
    const modelGenerator = new ModelGenerator(ttlFile,  outputFolder);
    await modelGenerator.parseSchema();
    modelGenerator.writeModels();
};
generate();

