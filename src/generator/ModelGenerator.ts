import * as fs from "fs";
import pkg from "rdf-parse";
import RdfSchema from "./RdfSchema.js";
import RdfDataProperty from "./RdfDataProperty.js";
import RdfObjectProperty from "./RdfObjectProperty.js";
import ModelDefinition from "./ModelDefinition.js";
//@ts-ignore
const rdfParser = pkg.default;

/**
 * Generates OGM models from an RDF schema file.
 */
export default class ModelGenerator {
    private readonly schemaFile: string;
    private readonly outputDir: string;
    private schema: RdfSchema;
    private definitions: { [key: string]: ModelDefinition } = {};

    constructor(schemaFile: string, outputDir: string) {
        this.schemaFile = schemaFile;
        this.outputDir = outputDir;
        this.schema = new RdfSchema();
    }

    async parseSchema() {
        console.log('Parsing schema...');
        return new Promise<void>((resolve, reject) => {
            const readableStream = fs.createReadStream(this.schemaFile);
            rdfParser.parse(readableStream, { contentType: 'text/turtle', baseIRI: '' })
                .on('data', (quad: any) => {
                    this.handleQuad(quad);
                })
                .on('end', () => {
                    console.log('Finished parsing schema.');
                    resolve();
                })
                .on('error', (err: any) => {
                    console.error('Error: ' + err);
                    reject(err);
                    process.exit(1);
                });
        });

    }

    private buildModelDefinition(className: string) {
        const definition = new ModelDefinition(className);

        const comment = this.schema.getClassComment(className);
        if (comment) {
            definition.setComment(comment);
        }

        //Find the parent classes from the hierarchy
        const parentClasses = this.schema.getParentClasses(className);
        parentClasses.forEach((parentClass: string) => {
            if (parentClass === className) {
                return;
            }
            definition.addParent(parentClass);
        });

        const dataProperties = this.schema.getDataProperties(className);
        dataProperties.forEach((property: RdfDataProperty) => {
            definition.addProperty(property);
        });

        const objectProperties = this.schema.getObjectProperties(className);
        objectProperties.forEach((property: RdfObjectProperty) => {
            definition.addRelationship(property);
        });

        this.definitions[className] = definition;
    }

    private buildModelDefinitions() {
        this.schema.getClasses().forEach((className: string) => {
            this.buildModelDefinition(className);
        });
    }

    public writeModels() {
        console.log('Writing models...');
        console.log('parsed classes', this.schema.getClasses());

        //Build individual model definitions
        this.buildModelDefinitions();

        this.schema.getClasses().forEach((className: string) => {
            this.writeModel(className);
        });
        console.log('Finished writing models.');
    }

    private handleQuad(quad: any) {
        console.log('handling quad...');
        //Strip the base IRI from the subject
        const subject = quad.subject.value.replace(/.+#/, '');

        //Strip the base IRI from the predicate
        const predicate = quad.predicate.value.replace(/.+#/, '');

        //Strip the base IRI from the object
        const object = quad.object.value.replace(/.+#/, '');
        console.log({ subject, predicate, object});

        //Handle class definitions
        if (predicate === 'type' && object === 'Class') {
            this.schema.addClass(subject);
        }

        //Handle subclassing
        else if (predicate === 'subClassOf') {
            this.schema.addSubclass(subject, object);
        }

        //Handle inverse names
        else if(predicate === 'inverseOf') {
            this.schema.addObjectPropertyInverseName(subject, object);
        }

        //Handle class comments
        else if (this.schema.isClass(subject) && predicate === 'comment') {
            this.schema.addClassComment(subject, object);
        }

        //Handle object properties
        else if (predicate === 'type' && object === 'ObjectProperty') {
            this.schema.addObjectProperty(subject);
        }

        //Handle object property domains
        else if (this.schema.isObjectProperty(subject) && predicate === 'domain') {
            this.schema.addObjectPropertyDomain(subject, object);
        }

        //Handle object property ranges
        else if (this.schema.isObjectProperty(subject) && predicate === 'range') {
            this.schema.addObjectPropertyRange(subject, object);
        }

        //Handle object property comments
        else if (this.schema.isObjectProperty(subject) && predicate === 'comment') {
            this.schema.addObjectPropertyComment(subject, '//' + object);
        }

        //Handle data properties
        else if (predicate === 'type' && object === 'DatatypeProperty') {
            this.schema.addDataProperty(subject);
        }

        //Handle data property domains
        else if (this.schema.isDataProperty(subject) && predicate === 'domain') {
            this.schema.addDataPropertyDomain(subject, object);
        }

        //Handle data property ranges
        else if (this.schema.isDataProperty(subject) && predicate === 'range') {
            this.schema.addDataPropertyRange(subject, object);
        }

        //Handle data property comments
        else if (this.schema.isDataProperty(subject) && predicate === 'comment') {
            this.schema.addDataPropertyComment(subject, '// ' + object);
        }
    }

    private getAllParents(className: string, allParents: string[]) {
        if (allParents.includes(className)) {
            return;
        }
        allParents.push(className);
        const parents = this.schema.getParentClasses(className);
        for (const parent of parents) {
            this.getAllParents(parent, allParents);
        }
    }

    private generateModel(className: any): string {
        //We need to take the properties, relationships, and imports from each parent
        //and add them to this class definition

        const definition = this.definitions[className];
        if (definition) {
            //Create a clone
            const fullDef = definition.clone();

            //Add in the parents
            let allParents: string[] = [];
            this.getAllParents(className, allParents);

            //Remove the class in case of a circular hierarchy
            allParents = allParents.filter((parent) => parent !== className);

            //Merge each parent with the class
            for (const parent of allParents) {
                const parentDefinition = this.definitions[parent];
                if (parentDefinition !== undefined) {
                    for (const parentProperty of parentDefinition.getProperties()) {
                        //Update comment to reflect origin
                        const clonedProp = parentProperty.clone();
                        let comment = clonedProp.getComment();
                        if (comment === '') {
                            comment = '//'
                        }
                        comment += ' (from ' + parent + ')';
                        clonedProp.setComment(comment);
                        fullDef.addProperty(clonedProp);
                    }

                    for (const parentRelationship of parentDefinition.getRelationships()) {
                        //Update comment to reflect origin
                        const clonedRelation = parentRelationship.clone();
                        let comment = clonedRelation.getComment();
                        if (comment === '') {
                            comment = '//'
                        }
                        comment += ' (from ' + parent + ')';
                        clonedRelation.setComment(comment);
                        fullDef.addRelationship(clonedRelation);
                    }
                }
            }
            return fullDef.generateModel();
        } else {
            return '';
        }
    }

    private writeModel(className: string) {
        const fileName = this.outputDir + '/' + className + '.ts';
        const fileContents = this.generateModel(className);
        if (fileContents !== '') {
            console.log('writing model to ' + fileName + '...');
            fs.writeFileSync(fileName, fileContents);
        }
    }
}