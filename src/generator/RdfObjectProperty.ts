export default class RdfObjectProperty {
    private name: string;
    private inverseName: string;
    private domain: string;
    private range: string;
    private comment: string;

    constructor(name: string, domain: string, range: string, comment: string | undefined = '', inverseName: string | undefined = '') {
        this.name = name;
        this.domain = domain;
        this.range = range;
        this.comment = comment;
        this.inverseName = inverseName;
    }

    public getName(): string {
        return this.name;
    }

    public getDomain(): string {
        return this.domain;
    }

    public getRange(): string {
        return this.range;
    }

    public getComment(): string {
        return this.comment;
    }

    public setName(name: string) {
        this.name = name;
    }

    public setDomain(domain: string) {
        this.domain = domain;
    }

    public setRange(range: string) {
        this.range = range;
    }

    public setComment(comment: string) {
        this.comment = comment;
    }

    setInverseName(inverseName: string) {
        this.inverseName = inverseName;
    }

    getInverseName(): string {
        return this.inverseName;
    }

    clone(): RdfObjectProperty {
        return new RdfObjectProperty(this.getName(), this.getDomain(), this.getRange(), this.getComment(), this.getInverseName());
    }
}