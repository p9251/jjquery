import RdfDataProperty from "./RdfDataProperty.js";
import RdfObjectProperty from "./RdfObjectProperty.js";

export default class RdfSchema {
    private classHierarchy: { [key: string]: string[] } = {};
    private classes: String[] = [];
    private classComments: { [key: string]: string } = {};
    private dataProperties: { [key: string]: RdfDataProperty } = {};
    private objectProperties: { [key: string]: RdfObjectProperty } = {};

    addClass(className: string) {
        //Ignore classes that are already defined
        if (!this.classes.includes(className)) {
            this.classes.push(className);
        }
    }

    addSubclass(subject: string, object: string) {
        this.addClass(subject);
        this.addClass(object);

        if (!this.classHierarchy[subject]) {
            this.classHierarchy[subject] = [];
        }
        (<string[]>this.classHierarchy[subject]).push(object);
    }

    public addDataProperty(name: string) {
        this.dataProperties[name] = new RdfDataProperty(name, '', '');
    }

    public addDataPropertyDomain(name: string, domain: string) {
        if (!this.dataProperties[name]) {
            throw new Error('Data property ' + name + ' not found.');
        }
        (<RdfDataProperty> this.dataProperties[name]).setDomain(domain);
    }

    public addDataPropertyRange(name: string, range: string) {
        if (!this.dataProperties[name]) {
            throw new Error('Data property ' + name + ' not found.');
        }
        (<RdfDataProperty> this.dataProperties[name]).setRange(range);
    }

    addDataPropertyComment(name: string, comment: string) {
        if (!this.dataProperties[name]) {
            throw new Error('Data property ' + name + ' not found.');
        }
        (<RdfDataProperty> this.dataProperties[name]).setComment(comment);
    }

    addObjectProperty(name: string) {
        this.objectProperties[name] = new RdfObjectProperty(name, '', '');
    }

    addObjectPropertyDomain(name: string, domain: string) {
        if (!this.objectProperties[name]) {
            throw new Error('Object property ' + name + ' not found.');
        }
        (<RdfObjectProperty> this.objectProperties[name]).setDomain(domain);
    }

    addObjectPropertyRange(name: string, range: string) {
        if (!this.objectProperties[name]) {
            throw new Error('Object property ' + name + ' not found.');
        }
        (<RdfObjectProperty> this.objectProperties[name]).setRange(range);
    }

    addObjectPropertyComment(name: string, comment: string) {
        if (!this.objectProperties[name]) {
            throw new Error('Object property ' + name + ' not found.');
        }
        (<RdfDataProperty> this.objectProperties[name]).setComment(comment);
    }

    addObjectPropertyInverseName(name: string, inverse: string) {
        if (!this.objectProperties[name]) {
            throw new Error('Object property ' + name + ' not found.');
        }
        (<RdfDataProperty> this.objectProperties[name]).setInverseName(inverse);
    }

    isObjectProperty(subject: any) {
        return this.objectProperties[subject] !== undefined;
    }

    isDataProperty(subject: any) {
        return this.dataProperties[subject] !== undefined;
    }

    getClasses() {
        return this.classes;
    }

    getParentClasses(className: string) {
        return this.classHierarchy[className] ?? [];
    }

    getDataProperties(className: string): RdfDataProperty[] {
        return Object.values(this.dataProperties).filter((property: RdfDataProperty) => {
            return property.getDomain() === className;
        });
    }

    getObjectProperties(className: string/*, includeInverse: boolean*/): RdfObjectProperty[] {
        const properties: RdfObjectProperty[] =  Object.values(this.objectProperties).filter((property: RdfObjectProperty) => {
            return property.getDomain() === className;
        });

        /*if (includeInverse) {
            for (const property of properties) {
                const inverseProperty = Object.values(this.objectProperties).find((prop: RdfObjectProperty) => prop.getInverseName() === property.getName());
                if (inverseProperty) {
                    properties.push(inverseProperty);
                }
            }
        }*/

        return properties;
    }

    isClass(className: string): boolean {
        return this.classes.includes(className);
    }

    addClassComment(className: string, comment: string) {
        this.classComments[className] = comment;
    }

    getClassComment(className: string): string | undefined {
        return this.classComments[className];
    }
}