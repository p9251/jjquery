import ITriple from "./ITriple.js";
import {ICondition} from "../query/condition/ICondition.js";
import NameResolution from "../query/naming/NameResolution.js";

export default class Triple implements ITriple, ICondition {
    protected subject: ICondition;
    protected predicate: ICondition;
    protected object: ICondition;

    constructor(subject: ICondition, predicate: ICondition, object: ICondition) {
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;
    }

    getSubject(): ICondition {
        return this.subject;
    }

    getPredicate(): ICondition {
        return this.predicate;
    }

    getObject(): ICondition {
        return this.object;
    }

    setSubject(subject: ICondition): void {
        this.subject = subject;
    }

    setPredicate(predicate: ICondition): void {
        this.predicate = predicate;
    }

    setObject(object: ICondition): void {
        this.object = object;
    }

    toString(nameResolution: NameResolution | undefined): string {
        //Resolve relationship and property names, relationships take precedence
        //todo: make it clear you shouldn't have conflicts here. If you do, you'd have to name them explicitly with the "?p_" prefix
        let subject = NameResolution.resolveFieldName(this.subject, nameResolution);
        let object = NameResolution.resolveFieldName(this.object, nameResolution);
        return `${subject} ${this.predicate} ${object}`;
    }
}