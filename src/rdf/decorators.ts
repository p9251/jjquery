import {Resource} from "./classes/resources.js";
import {Relation} from "./relation.js";
import Repository from "../repository.js";
import {JJQuery} from "../jjquery.js";

//@ts-ignore
export const parent = (parentClass: Function) => {
    //@ts-ignore
    return (target: Function) => {

    }
}

// @ts-ignore
export const relation = (relationship: string, resourceClass: string) => {
    return function (target: any, property: string) {
        const className = target.constructor.name;
        async function relationshipLoader(connection: JJQuery, model: Resource): Promise<Relation<Resource>> {
            //todo: fix this hack
            await JJQuery.getClassLoader().load(resourceClass);
            const repo = new Repository<Resource>(resourceClass, connection);
            return new Relation<Resource>(model, relationship, repo);
        };
        target.setRelationshipLoader(className, relationship, relationshipLoader);

        Object.defineProperty(target, property, {
            get: function () {
                return this.getRelatedModels(relationship);
            },
            set: function (value: any) {
                console.error('relationship.set is not supported', value);
            }
        });
    };
}

export const property = (rdfType: string) => {
    return function (target: Resource, property: string) {
        const className = target.constructor.name;
        //@ts-ignore
        const defaultValue = target[property];

        target.setDefaultProperty(className, property, defaultValue);
        target.setPropertyType(className, property, rdfType);

        Object.defineProperty(target, property, {
            get: function () {
                return this.getProperty(property);
            },
            set: function (value: any) {
                this.setProperty(property, value);
            }
        });
    };
}

//@ts-ignore
export const subject = (target: any, property: string) => {
}

//@ts-ignore
export const object = (target: any, property: string) => {
}

//@ts-ignore
export const predicate = (target: any, property: string) => {
}