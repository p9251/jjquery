import {ICondition} from "../query/condition/ICondition.js";

export default interface ITriple {
    getSubject(): ICondition;
    setSubject(subject: ICondition): void;

    getPredicate(): ICondition;
    setPredicate(predicate: ICondition): void;

    getObject(): ICondition;
    setObject(object: ICondition): void;
}