import {Resource} from "./classes/resources.js";
import Repository from "../repository.js";
import {QueryCondition} from "../query/condition/QueryCondition.js";

export class Relation<T extends Resource> {
    private subject: Resource;
    private relationship: string;
    private repo: Repository<T>;

    //The in-memory models for the relationship
    private models: T[]|undefined = undefined;

    constructor(subject: Resource, relationship: string, repo: Repository<T>) {
        this.subject = subject;
        this.relationship = relationship;
        this.repo = repo;
    }

    addModel(model: T): void {
        if (!this.models) {
            this.models = [];
        }
        this.models.push(model);
    }

    setModels(models: T[]|undefined): void {
        this.models = models;
    }

    async getModelTemplate(): Promise<Resource> {
        return this.repo.getTemplate();
    }

    async getModelInstance(): Promise<Resource> {
        return this.repo.newInstance();
    }

    getSubject(): Resource {
        return this.subject;
    }

    getPredicate(): string {
        return this.relationship;
    }

    async getModels(): Promise<T[]> {
        if (this.models === undefined) {
            //todo: cache the models for this relationship
            let subject = ':' + this.subject.iri;
            let predicate = this.relationship;
            let object = '?_iri';

            if (predicate.startsWith('-')) {
                //reverse subject and object
                const tmpSubject = subject;
                subject = object;
                object = tmpSubject;
                predicate = predicate.substring(1);
            }

            const condition = new QueryCondition(subject, `:${predicate}`, object);
            this.setModels(await this.repo.findByCondition(condition));
        }

        return this.models || [];
    }

    getLoadedModels(): T[]|undefined {
        return this.models;
    }
}
