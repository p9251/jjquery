import TypeMapping from "./TypeMapping.js";

export default class DataFormatter {
    static formatAsRdf(value: any, rdfType: string): string|null {
        if (value === null || value === undefined) {
            return null;
        }
        value = value.toString();
        value.replace('\\', '\\\\');
        value.replace('"', '\\"');
        return '"' + value + '"^^xsd:' + rdfType;
    }

    static parseFromRdf(value: string, rdfType: string): any {
        const type = TypeMapping.getTypescriptType(rdfType);
        switch (type) {
            case 'boolean':
                return value === 'true';
            case 'number':
                return Number(value);
            case 'Date':
                return new Date(value);
            case 'string':
            default:
                return value;
        }
    }
}