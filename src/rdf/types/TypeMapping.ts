//todo: add all of the other valid types
export default class TypeMapping {
    //Some types can be stored in RDF but aren't anything but strings in SPARQL (afaik)
    private static rdfOnlyTypes: string[] = ['anyURI', 'base64Binary', 'hexBinary', 'Name', 'NCName', 'NMTOKEN', 'token', 'gYear', 'gYearMonth', 'gMonth', 'gDay', 'gMonthDay'];

    static getTypescriptType(rdfType: string): string {
        //todo: make sure we're handling all of the unsigned and byte types correctly (are bytes numbers?)
        switch (rdfType) {
            case 'boolean':
                return 'boolean';
            case 'integer':
            case 'double':
            case 'float':
            case 'decimal':
            case 'nonPositiveInteger':
            case 'negativeInteger':
            case 'long':
            case 'int':
            case 'short':
            case 'nonNegativeInteger':
            case 'unsignedLong':
            case 'unsignedInt':
            case 'unsignedShort':
            case 'byte':
            case 'unsignedByte':
            case 'positiveInteger':
                return 'number';
            case 'dateTime':
            case 'date':
            case 'time':
                return 'Date';
            case 'string':
            case 'language':
            case 'anyURI':
            case 'base64Binary':
            case 'hexBinary':
            case 'gYear':
            case 'gYearMonth':
            case 'gMonth':
            case 'gDay':
            case 'gMonthDay':
            case 'Name':
            case 'NCName':
            case 'NMTOKEN':
            case 'token':
            case 'time':
                return 'string';
            default:
                return 'string';
        }
    }

    static getSparqlType(rdfType: string): string {
        if (TypeMapping.rdfOnlyTypes.includes(rdfType)) {
            return 'string';
        }
        return rdfType;
    }

    static guessRdfType(value: any): string {
        switch (typeof value) {
            case 'boolean':
                return 'boolean';
            case 'number':
                if (Number.isInteger(value)) {
                    return 'integer';
                } else {
                    return 'double';
                }
            case 'object':
                if (value instanceof Date) {
                    return 'dateTime';
                } else {
                    return 'object';
                }
            case 'string':
            default:
                return 'string';
        }
    }
}