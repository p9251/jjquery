import {Relation} from "../relation.js";
import {JJQuery} from "../../jjquery.js";

export class Resource {
    private __new: boolean = true;

    public iri: string = '';
    public type: string = '';


    protected static __defaultRelationshipLoaders: { [key: string]: { [key: string]: (connection: JJQuery, model: Resource) => Promise<Relation<Resource>> } } = {};
    protected static __defaultDataProperties: { [key: string]: { [key: string]: any } } = {};
    protected static __dataPropertyTypes: { [key: string]: { [key: string]: string } } = {};
    /**
     * The model's defined relationships.
     *
     * @protected
     */
    protected __relationships: { [key: string]: Relation<Resource> } = {};

    /**
     * The data properties that were loaded from the DB.
     *
     * @private
     */
    private __loadedDataProperties: { [key: string]: any } = {};

    /**
     * The current data properties.
     *
     * @private
     */
    private __dataProperties: { [key: string]: any } = {};

    constructor() {
        this.init();
    }

    private init() {
        //Initialize the data properties
        this.__dataProperties = { ...this.getDefaultProperties(this.constructor.name) };

        //Initialize the relationships
        //These are only loaded once they are requested for now
    }

    isNew(): boolean {
        return this.__new;
    }

    setNew(__new: boolean) {
        this.__new = __new;
    }

    getType(): string {
        return this.constructor.name;
    }

    public async getRelatedModels(name: string): Promise<Resource[] | undefined> {
        if (!this.__relationships[name]) {
            throw new Error(`No relationship found for ${name}`);
        }
        // @ts-ignore
        return this.__relationships[name].getModels();
    }

    private getDefaultProperties(className: string): { [key: string]: any } {
        return Resource.__defaultDataProperties[className] ?? {};
    }

    setDefaultProperty(className: string, property: string, value: any) {
        if (Resource.__defaultDataProperties[className] === undefined) {
            Resource.__defaultDataProperties[className] = {};
        }
        //@ts-ignore
        Resource.__defaultDataProperties[className][property] = value;
    }

    setRelationshipLoader(className: string, name: string, loader: (connection: JJQuery) => Promise<Relation<Resource>>) {
        if (Resource.__defaultRelationshipLoaders[className] === undefined) {
            Resource.__defaultRelationshipLoaders[className] = {};
        }
        //@ts-ignore
        Resource.__defaultRelationshipLoaders[className][name] = loader;
    }

    setPropertyType(className: string, property: string, type: string) {
        if (Resource.__dataPropertyTypes[className] === undefined) {
            Resource.__dataPropertyTypes[className] = {};
        }
        //@ts-ignore
        Resource.__dataPropertyTypes[className][property] = type;
    }

    getPropertyType(property: string): string|undefined {
        if (Resource.__dataPropertyTypes[this.constructor.name] === undefined) {
            return undefined;
        } else {
            //@ts-ignore
            return Resource.__dataPropertyTypes[this.constructor.name][property];
        }
    }

    setProperty(name: string, value: any) {
        if (Object.keys(this.__dataProperties).includes(name)) {
            //We only add properties that exist in the model
            this.__dataProperties[name] = value;
        }
    }

    setProperties(properties: { [key: string]: any }) {
        Object.keys(properties).forEach((prop) => {
            this.setProperty(prop, properties[prop]);
        });
    }

    getProperty(name: string): any {
        return this.__dataProperties[name];
    }

    getProperties(): { [key: string]: any } {
        return this.__dataProperties;
    }

    /**
     * Loads properties from the DB
     *
     * @param properties
     */
    loadProperties(properties: { [key: string]: any }) {
        this.__loadedDataProperties = { ...properties };
        this.__dataProperties = { ...properties };
    }

    /**
     * Gets the modified object properties for creating and updating.
     */
    getModifiedProperties(): { [key: string]: any } {
        const modified: { [key: string]: any } = {};
        for (const prop of Object.keys(this.__dataProperties)) {
            if (this.__loadedDataProperties[prop] !== this.__dataProperties[prop]) {
                modified[prop] = this.__dataProperties[prop];
            }
        }
        return modified;
    }

    getRelationshipLoaders(): { [key: string]: (connection: JJQuery, model: Resource) => Promise<Relation<Resource>> } {
        return Resource.__defaultRelationshipLoaders[this.constructor.name] || {};
    }

    getRelationshipLoader(name: string): (connection: JJQuery,model: Resource) => Promise<Relation<Resource>|undefined> {
        // @ts-ignore
        return Resource.__defaultRelationshipLoaders[this.constructor.name][name];
    }

    getRelationship(name: string): Relation<Resource>|undefined {
        // @ts-ignore
        return this.__relationships[name];
    }

    getRelationships(): { [key: string]: Relation<Resource> } {
        return this.__relationships;
    }

    setRelationship(name: string, relationship: Relation<Resource>) {
        this.__relationships[name] = relationship;
    }
}

export class Statement extends Resource {

}

export class Class extends Resource {

}