# jjquery-ogm
A lightweight ORM for use with RDF databases.

#### This package is currently in alpha and undergoing heavy development. It's not meant for use yet, but if you're interested in the project and want to contribute, you can reach out via email (sheldonjuncker [at] gmail [dot] com.

At some point in the upcoming weeks, I'll be releasing a beta version.

The gist of what that beta will do is:
1. Code-gen Typescript models to represent an RDF schema from a Turtle file
2. Allow basic CRUD operations using the models
3. Handle data properties and object relationships
4. Provide basic query building

#### Todo before beta: 
1. Support raw SPARQL queries using the models
2. Handle basic data types
3. Support namespaces
4. Saving of relational data
5. One-to-many and many-to-many relationships

